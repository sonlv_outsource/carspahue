CREATE TABLE IF NOT EXISTS `#__booking` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`created_time` DATETIME NOT NULL ,
`modified_time` DATETIME NOT NULL ,
`booker` VARCHAR(16)  NOT NULL ,
`phone` VARCHAR(16)  NOT NULL ,
`booking_date` DATETIME NOT NULL ,
`info` VARCHAR(255)  NOT NULL ,
`note` TEXT NOT NULL ,
`status_id` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8_general_ci;

