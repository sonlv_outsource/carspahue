<?php
function getDistance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
{
    $lat1 = (double)$lat1;
    $lon1 = (double)$lon1;
    $lat2 = (double)$lat2;
    $lon2 = (double)$lon2;
    // Get latitude and longitude from the geodata
    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
    } else {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
$lat1 = 21.5832057;
$lon1 = 105.8176506;
$lat2 = 21.5930964802915;
$lon2 = 105.8363773802915;
echo getDistance($lat1, $lon1, $lat2, $lon2);
