<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Book
 * @author     vst <lvson1087@gmail.com>
 * @copyright  2019 @ Bizappco
 * @license    bản quyền mã nguồn mở GNU phiên bản 2
 */
defined('_JEXEC') or die;

JLoader::register('BookHelper', JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_book' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'book.php');

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Model\BaseDatabaseModel;

/**
 * Class BookFrontendHelper
 *
 * @since  1.6
 */
class BookHelpersBook
{
	/**
	 * Get an instance of the named model
	 *
	 * @param   string  $name  Model name
	 *
	 * @return null|object
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_book/models/' . strtolower($name) . '.php'))
		{
			require_once JPATH_SITE . '/components/com_book/models/' . strtolower($name) . '.php';
			$model = BaseDatabaseModel::getInstance($name, 'BookModel');
		}

		return $model;
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = Factory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

    /**
     * Gets the edit permission for an user
     *
     * @param   mixed  $item  The item
     *
     * @return  bool
     */
    public static function canUserEdit($item)
    {
        $permission = false;
        $user       = Factory::getUser();

        if ($user->authorise('core.edit', 'com_book'))
        {
            $permission = true;
        }
        else
        {
            if (isset($item->created_by))
            {
                if ($user->authorise('core.edit.own', 'com_book') && $item->created_by == $user->id)
                {
                    $permission = true;
                }
            }
            else
            {
                $permission = true;
            }
        }

        return $permission;
    }
}
