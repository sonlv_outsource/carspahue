<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Book
 * @author     vst <lvson1087@gmail.com>
 * @copyright  2019 @ Bizappco
 * @license    bản quyền mã nguồn mở GNU phiên bản 2
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_book.' . $this->item->id);

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_book' . $this->item->id))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>
<h1>Lịch hẹn #<?php echo $this->item->id; ?></h1>
<div class="item_fields detail-booking">

	<table class="table">




		<tr>
			<th><?php echo JText::_('COM_BOOK_FORM_LBL_BOOKING_BOOKER'); ?></th>
			<td><?php echo $this->item->booker; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_BOOK_FORM_LBL_BOOKING_PHONE'); ?></th>
			<td><?php echo $this->item->phone; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_BOOK_FORM_LBL_BOOKING_BOOKING_DATE'); ?></th>
			<td><?php echo $this->item->booking_date; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_BOOK_FORM_LBL_BOOKING_INFO'); ?></th>
			<td><?php echo $this->item->info; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_BOOK_FORM_LBL_BOOKING_NOTE'); ?></th>
			<td><?php echo nl2br($this->item->note); ?></td>
		</tr>

		<!-- <tr>
			<th><?php echo JText::_('COM_BOOK_FORM_LBL_BOOKING_CREATED_TIME'); ?></th>
			<td><?php echo $this->item->created_time; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_BOOK_FORM_LBL_BOOKING_MODIFIED_TIME'); ?></th>
			<td><?php echo $this->item->modified_time; ?></td>
		</tr> -->

		<tr>
			<th><?php echo JText::_('COM_BOOK_FORM_LBL_BOOKING_STATUS_ID'); ?></th>
			<td><?php echo $this->item->status_id; ?></td>
		</tr>

	</table>

</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_book&task=booking.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_BOOK_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_book.booking.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_BOOK_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_BOOK_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_BOOK_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_book&task=booking.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_BOOK_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>
