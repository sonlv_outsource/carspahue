<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Book
 * @author     vst <lvson1087@gmail.com>
 * @copyright  2019 @ Bizappco
 * @license    bản quyền mã nguồn mở GNU phiên bản 2
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;

HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('bootstrap.tooltip');
HTMLHelper::_('behavior.multiselect');
HTMLHelper::_('formbehavior.chosen', 'select');

$user       = Factory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_book') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'bookingform.xml');
$canEdit    = $user->authorise('core.edit', 'com_book') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'bookingform.xml');
$canCheckin = $user->authorise('core.manage', 'com_book');
$canChange  = $user->authorise('core.edit.state', 'com_book');
$canDelete  = $user->authorise('core.delete', 'com_book');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_book/css/list.css');
?>

<form action="<?php echo htmlspecialchars(Uri::getInstance()->toString()); ?>" method="post"
      name="adminForm" id="adminForm">

	<?php //echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>
        <div class="table-responsive-2">
          <h1 class="customer-box__head--title">Danh sách đặt lịch</h1>
          <?php if ($canCreate) : ?>
        		<a style="float:right" href="index.php?Itemid=366"
        		   class="btn btn-secondary btnbooking"><i
        				class="icon-plus"></i>
        			<?php //echo Text::_('COM_BOOK_ADD_ITEM'); ?>ĐẶT LỊCH</a>
        	<?php endif; ?>

	<table class="table table-striped" id="bookingList">
		<thead>
		<tr>
			<?php if (isset($this->items[0]->state)): ?>

			<?php endif; ?>

							<th class='' width="5%">
				<?php echo JHtml::_('grid.sort',  'COM_BOOK_BOOKINGS_ID', 'a.id', $listDirn, $listOrder); ?>
				</th>
				<!-- <th class=''>
				<?php echo JHtml::_('grid.sort',  'COM_BOOK_BOOKINGS_CREATED_TIME', 'a.created_time', $listDirn, $listOrder); ?>
				</th> -->
				<th class='' width="15%">
				<?php echo JHtml::_('grid.sort',  'COM_BOOK_BOOKINGS_BOOKER', 'a.booker', $listDirn, $listOrder); ?>
				</th>
				<th class='' width="15%">
				<?php echo JHtml::_('grid.sort',  'COM_BOOK_BOOKINGS_PHONE', 'a.phone', $listDirn, $listOrder); ?>
				</th>
				<th class='' width="15%">
				<?php echo JHtml::_('grid.sort',  'COM_BOOK_BOOKINGS_BOOKING_DATE', 'a.booking_date', $listDirn, $listOrder); ?>
				</th>
				<th class='' width="20%">
				<?php echo JHtml::_('grid.sort',  'Dòng xe', 'a.info', $listDirn, $listOrder); ?>
				</th>
				<th class='' width="25%">
				<?php echo JHtml::_('grid.sort',  'COM_BOOK_BOOKINGS_NOTE', 'a.note', $listDirn, $listOrder); ?>
				</th>
				<th class='' width="5%">
				<?php echo JHtml::_('grid.sort',  'COM_BOOK_BOOKINGS_STATUS_ID', 'a.status_id', $listDirn, $listOrder); ?>
				</th>


							<?php if ($canEdit || $canDelete): ?>
					<th class="center">
				<?php echo JText::_('COM_BOOK_BOOKINGS_ACTIONS'); ?>
				</th>
				<?php endif; ?>

		</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->items as $i => $item) : ?>
			<?php $canEdit = $user->authorise('core.edit', 'com_book'); ?>

							<?php if (!$canEdit && $user->authorise('core.edit.own', 'com_book')): ?>
					<?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
				<?php endif; ?>

			<tr class="row<?php echo $i % 2; ?>">

				<?php if (isset($this->items[0]->state)) : ?>
					<?php $class = ($canChange) ? 'active' : 'disabled'; ?>

				<?php endif; ?>

								<td>

					<?php echo $item->id; ?>
				</td>
				<!-- <td>

					<?php echo $item->created_time; ?>
				</td> -->
				<td>
				<?php if (isset($item->checked_out) && $item->checked_out) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'bookings.', $canCheckin); ?>
				<?php endif; ?>
				<a href="<?php echo JRoute::_('index.php?option=com_book&view=booking&id='.(int) $item->id); ?>">
				<?php echo $this->escape($item->booker); ?></a>
				</td>
				<td>

					<?php echo $item->phone; ?>
				</td>
				<td>

					<?php echo $item->booking_date; ?>
				</td>
				<td>

					<?php echo $item->info; ?>
				</td>
				<td>

					<?php echo $item->note; ?>
				</td>
				<td>

					<?php echo $item->status_id; ?>
				</td>


								<?php if ($canEdit || $canDelete): ?>
					<td class="center">
						<?php if ($canEdit): ?>
							<a  href="<?php echo JRoute::_('index.php?option=com_book&task=bookingform.edit&id=' . $item->id, false, 2); ?>" class="btn btn-mini" type="button"><i class="icon-edit" ></i></a>
						<?php endif; ?>
						<?php if ($canDelete): ?>
							<a  href="<?php echo JRoute::_('index.php?option=com_book&task=bookingform.remove&id=' . $item->id, false, 2); ?>" class="btn btn-mini delete-button" type="button"><i class="icon-trash" ></i></a>
						<?php endif; ?>
					</td>
				<?php endif; ?>

			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
        </div>


	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
	<?php echo HTMLHelper::_('form.token'); ?>
</form>

<?php if($canDelete) : ?>
<script type="text/javascript">

	jQuery(document).ready(function () {
		jQuery('.delete-button').click(deleteItem);
	});

	function deleteItem() {

		if (!confirm("<?php echo Text::_('COM_BOOK_DELETE_MESSAGE'); ?>")) {
			return false;
		}
	}
</script>
<?php endif; ?>
