<?php
/**
 * @version		1.3.1
 * @package		Joomla
 * @subpackage	EShop
 * @author  	Giang Dinh Truong
 * @copyright	Copyright (C) 2012 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die();

abstract class modEshopCategoryHelper
{
	/**
	 *
	 * Function to get Categories
	 * @return array|categories
	 */
	public static function getCategories($categoryId)
	{
        $conf = JFactory::getConfig();

        // Setting a location for your cached data.
        $cacheBase = JPATH_SITE . '/cache';

        // Your custom cachegroup
        $cacheGroup = 'homecache';

        // Lifetime for your cache
        $lifetime = 60;

        // Setting your options
        $options = array(
            'defaultgroup'  => $cacheGroup,
            'storage'       => $conf->get('cache_handler', ''),
            'caching'       => true,
            'cachebase'     => $cacheBase,
            'lifetime'      => $lifetime,
        );

        // Instantiate your cache object
        $cache = \JCache::getInstance('', $options);

        // Create $cacheDataId
        $cacheDataId = 'category_tree_'.JFactory::getLanguage()->getTag();
        // It's time to check for cached data
        if ($cache->get($cacheDataId) !== false )
        {
            $categories = $cache->get($cacheDataId); // We got data from cache
        }
        else
        {

		    $categories = EshopHelper::getCategoriesTree(JFactory::getLanguage()->getTag(), true);
            $cache->store($categories, $cacheDataId);
        }
		return $categories;
	}

	/**
	 *
	 * Function to get id of parent category
	 * @param int $categoryId
	 * @return int id of parent category
	 */
	public static function getParentCategoryId($categoryId)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query = $query->select('category_parent_id')
			->from('#__eshop_categories')
			->where('id = ' . $categoryId);
		$db->setQuery($query);
		return $db->loadResult() ? $db->loadResult() : $categoryId;
	}
}
