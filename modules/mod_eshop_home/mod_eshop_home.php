<?php
/**
 * @version		1.3.1
 * @package		Joomla
 * @subpackage	EShop
 * @author  	Giang Dinh Truong
 * @copyright	Copyright (C) 2011 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;
JLoader::registerNamespace('api', JPATH_PLUGINS . '/api', false, false, 'psr4');
require_once (dirname(__FILE__).'/helper.php');
require_once JPATH_ROOT . '/administrator/components/com_eshop/libraries/defines.php';
require_once JPATH_ROOT . '/administrator/components/com_eshop/libraries/inflector.php';
require_once JPATH_ROOT . '/administrator/components/com_eshop/libraries/autoload.php';
$currency = new EshopCurrency();
$tax = new EshopTax(EshopHelper::getConfig());
$modules = modEshopHomeHelper::getHomeProduct();


require JModuleHelper::getLayoutPath('mod_eshop_home', $params->get('layout', 'default'));
