<?php
/**
 * @package     api\model
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

namespace api\model;


class Sconfig
{
    public $hotline = '0935888225';
    public $siteName = 'CarSpa Huế';
    public $deeplink = 'carspa';
    public $onesignalAppKey = '00b720e5-277a-411e-883e-160256d59c4f';
    public $onesignalRestKey = 'MTNkYmM0NmUtYjk2ZC00ZTI5LWI3ZGMtM2UxOGZiOWIzNzRi';
    public $address = '1 Lê Minh, An Đông, Thành phố Huế, Thừa Thiên Huế';
    public $service = array(
        array('text' => 'Được kiểm tra hàng trước khi thanh toán', 'icon' => 'ios-checkbox-outline'),
        array('text' => 'Được tích điểm giảm giá', 'icon' => 'ios-checkbox-outline'),
        array('text' => 'Giao hàng nhanh chóng', 'icon' => 'ios-checkbox-outline')
    );

    public $orderBeginStatus = 8;
    public $orderCancleStatus = 1;
    public $canCancelStatus = array(
        '8'
    );

    public $shippingLimitArea = array(
        -1
    );
    public $shippingLimitCategory = array();
    // public $shippingLimitCategory = array(
    //     3,
    //     213,
    //     214,
    //     29,
    //     215,
    //     216,
    //     217,
    //     30,
    //     161,
    //     159,
    //     278,
    //     279,
    //     280,
    //     281,
    //     284,
    //     285,
    //     286,
    //     287,
    //     288,
    //     289,
    //     360,
    //     294,
    //     295,
    //     297,
    //     298,
    //     299,
    //     301,
    //     303
    // );

    public $shippingLimitMessage = '';
    public $minCartAmount = 100000;
    public $google_api_key = 'AIzaSyDh1rJHvJAmUFKjvCXpzefDp15lfdpT8So';
    public $shop_longlat = array(
        'lat' => 21.5930964802915,
        'lng' => 105.8363773802915
    );

    public $email_prefix = 'carspa';
    public $email_suffix = 'carspahue.com';

    // INSERT INTO `prfwj_eshop_shippings` (`name`, `title`, `author`, `creation_date`, `copyright`, `license`, `author_email`, `author_url`, `version`, `description`, `params`, `ordering`, `published`) VALUES('eshop_bizappco','Minh Cau Mart','Giang Dinh Truong','2019-11-04 05:51:37','Copyright 2013 Ossolution Team','http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU/GPL version 2','contact@joomdonation.com','www.joomdonation.com','1.0.0','This is UPS Shipping method for Eshop','{\"shipping_area\":\"50\",\"shipping_name\":\"Minh cau\"}','1','1');

}
