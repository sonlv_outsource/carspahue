<?php
class plgAPISync extends ApiPlugin
{
    public function __construct(&$subject, $config = array())
    {
        parent::__construct($subject, $config = array());

        ApiResource::addIncludePath(dirname(__FILE__) . '/sync');


        // Set the login resource to be public
        $this->setResourceAccess('price', 'public', 'GET');
        $this->setResourceAccess('stock', 'public', 'GET');
        //$this->setResourceAccess('token', 'public', 'GET');

    }
}