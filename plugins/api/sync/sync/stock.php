<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 4/27/2019
 * Time: 9:51 AM
 */


use api\model\dao\shop\ShopCustomerDao;
use api\model\form\SyncStockForm;
use api\model\SUtil;

defined('_JEXEC') or die('Restricted access');
jimport('joomla.user.user');

class SyncApiResourceStock extends ApiResource
{
    /**
     * @OA\Get(
     *     path="/api/sync/stock",
     *     tags={"Đồng bộ sản phẩm"},
     *     summary="Cập nhật số lượng sản phẩm tồn kho ",
     *     description="Cập nhật số lượng sản phẩm tồn kho",
     *     operationId="post",
     *     @OA\Parameter(
     *         name="token",
     *         in="query",
     *         description="Token",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *           default=""
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="product_sku",
     *         in="query",
     *         description="Mã sản phẩm",
     *         required=true,
     *         @OA\Schema(
     *           type="string",
     *           default="8935008890556"
     *         ),
     *         style="form"
     *     ),
     *     @OA\Parameter(
     *         name="product_quantity",
     *         in="query",
     *         description="Số lượng tồn kho ",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           default="100"
     *         ),
     *         style="form"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="{  'err_msg': '',  'err_code': '',  'response_id': '',  'api': '',  'version': '',  'data': 'Nội dung thông báo' }",
     *         @OA\Schema(ref="#/components/schemas/ErrorModel"),
     *     ),
     *     @OA\Response(
     *         response="401",
     *        description="{  'err_msg': 'Nôi dung lỗi',  'err_code': 403,  'response_id': '',  'api': '',  'version': '',  'data': {} }",
     *     )
     * )
     */


    public function get()
    {
        $data = $this->getRequestData();
        $uid = SUtil::decodeUserToken($data['token']);
        if(!$uid){
            ApiError::raiseError('401', 'Yêu cầu không hợp lệ.');
            return false;
        }
        $id = $_SERVER['REMOTE_ADDR'];
        $allowIps = array(
            '117.4.80.165',
            '14.170.155.0'
        );
        if(!in_array($id, $allowIps)){
            ApiError::raiseError('403', 'Not found.');
            return false;
        }
        $user = JFactory::getUser($uid);
        if ($user->id) {
            $form = new SyncStockForm();
            $form->setAttributes($data);
            if ($form->validate()) {
                $db = JFactory::getDbo();
                $date = JFactory::getDate()->toSql();
                $fields = array(
                    $db->quoteName('product_quantity') . ' = ' . (int)$data['product_quantity'],
                    $db->quoteName('created_by') . ' = ' . (int)$user->id,
                    $db->quoteName('stock_modified_date') . ' = ' . $db->quote($date),
                );
                $conditions = array(
                    $db->quoteName('product_sku') . ' = ' . $db->quote($data['product_sku']),
                );
                $query = $db->getQuery(true);
                $query->update($db->quoteName('#__eshop_products'))->set($fields)->where($conditions);
                $db->setQuery($query);
                $db->execute();
                $numb = $db->getAffectedRows();
                $this->plugin->setResponse($numb);
                return true;
            } else {
                ApiError::raiseError('401', $form->getFirstError());
                return false;
            }
        }
        ApiError::raiseError('401', 'Yêu cầu không hợp lệ.');
        return false;
    }


}
