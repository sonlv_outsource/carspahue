<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 4/27/2019
 * Time: 9:51 AM
 */

use api\model\dao\shop\ShopAddressDao;
use api\model\dao\shop\ShopOrderDetailDao;
use api\model\dao\shop\ShopOrderProductDao;

require_once(JPATH_SITE . '/components/com_eshop/helpers/helper.php');
defined('_JEXEC') or die('Restricted access');
jimport('joomla.user.user');

class UsersApiResourceShopshipghtk extends ApiResource
{
	private $token;
	private $url = 'https://dev.ghtk.vn/services';
	private $data;

    static public function routes()
    {
        $routes[] = 'shopshipghtk/';

        return $routes;
    }
    /**
     * @OA\Get(
     *     path="/api/users/shopshipghtkfee",
     *     tags={"Shop"},
     *     summary="Get shop shippings",
     *     description="Change password user",
     *     operationId="post",
     *     security = { { "bearerAuth": {} } },
     *     @OA\Response(
     *         response=200,
     *         description="successful login",
     *         @OA\Schema(ref="#/components/schemas/ErrorModel"),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid request",
     *     )
     * )
     */
    public function __construct(ApiPlugin $plugin)
    {
        $this->token = EshopHelper::getConfigValue('ghtk_api_key');
        parent::__construct($plugin);
    }

    public function get()
    {
	    $input = JFactory::getApplication()->input;   // get params
	    $data = $input->get->getArray();
	    $shopPickData = $this->shopInfo();
	    //GET data
	    if ($data) {
		    $this->data = $data;
	    }
		$this->data['pick_province'] = $shopPickData['shopProvince'];
		$this->data['pick_district'] = $shopPickData['shopDistrict'];
	    if ($data['type'] && $data['type'] == 'status'){
		    $this->plugin->setResponse($this->status());
	    }elseif ($data['type'] && $data['type'] == 'cancel'){
	        $this->plugin->setResponse($this->cancel());
        }else{
		    $this->plugin->setResponse($this->fee());
	    }
    }
	public function post()
	{
       $data = $this->getRequestData();
       if ($data['partner_id']){
           $this->plugin->setResponse($this->cancel());
       }else{
           $this->plugin->setResponse($this->order());
       }
	}

	private function fee() {
		$url = $this->url . '/shipment/fee?' . http_build_query($this->data);
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_HTTPHEADER => array(
				"Token: " . $this->token,
			),
		));
		$result = curl_exec($curl);
		curl_close($curl);
		$response = null;
		if ($result){
			$response = json_decode($result);
		}
		if ($response->success){
			$response = $response->fee;
		}
		return $response;
	}

	private function order() {
        $data = $this->getRequestData();
		// general data request to ghtk
		$infoOrder = $this->generalOrderInfo($data);
		$url = $this->url . '/shipment/order';
		$req = json_encode($infoOrder);
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $req,
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Token: " . $this->token,
				"Content-Length: " . strlen($req),
			),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		return $response;
	}

	private function status() {
		if ( isset($this->data['partner_id']) && $this->data['partner_id'] ) {
			$code = '/shipment/partner_id:' . trim($this->data['partner_id']);
			unset($this->data['partner_id']);
		} else if ( isset($this->data['code']) && $this->data['code'] ) {
			$code = '/shipment/' . trim($this->data['code']);
		} else {
			return array('success'=>false, 'message'=>'Code?');
		}

		$url = $this->url . $code;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_HTTPHEADER => array(
				"Token: " . $this->token,
			),
		));

		$response = curl_exec($curl);
		curl_close($curl);

		return $response;
	}

	private function cancel() {
        $data = $this->getRequestData();
        $code = null;
		if ($data['partner_id']) {
			$code = '/partner_id:' . trim($data['partner_id']);
		}
		$url = $this->url . '/shipment/cancel' . $code;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_HTTPHEADER => array(
				"Token: " . $this->token,
			),
		));

		$response = curl_exec($curl);
		curl_close($curl);

		return json_decode($response);
	}

	private function shopInfo(){
		$countryId = intval(\EshopHelper::getConfigValue('country_id'));
		$zoneId =  intval(\EshopHelper::getConfigValue('zone_id'));
		$telephone =  \EshopHelper::getConfigValue('telephone');
		$address = \EshopHelper::getConfigValue('address');
		$province = \EshopHelper::getCountry($countryId);
		$district = \EshopHelper::getZone($zoneId);
		$result = array(
			'shopProvince' => $province->country_name,
			'shopDistrict' => $district->zone_name,
			'shopTelephone' => $telephone,
			'shopAddress' => $address,
		);
		return $result;
	}

	private function generalOrderInfo($data){
		$orderId = intval($data['orderId']);
		$pickDate = $data['pickDate'] ? $data['pickDate'] :  date('Y-m-d');
		// address of shop in config eshop
		$shopAddress = $this->shopInfo();
		$address = \EshopHelper::getConfigValue('address');
		$storeName = \EshopHelper::getConfigValue('store_name');
		$oderDetail = \EshopHelper::getOrder($orderId);
		$orderProductDao = new ShopOrderProductDao();
		$params = array();
		$params['where'][] = 'o.order_id = '.$orderId;

		// get list products of order
		$orderProducts = $orderProductDao->getProducts($params);
		$orders = [];
		foreach($orderProducts as &$item){
			$temp = array(
				'name' => $item->product_name,
				'weight' => $item->product_weight > 0 ? $item->product_weight : '0.1',
				'quantity' => $item->quantity
			);
			$orders['products'][] = $temp;
		}

		$orderTotal = \EshopHelper::getOrderTotals($orderId);
		$subTotal = 0;
		foreach ($orderTotal as $item)
		{
			if ($item->name == 'sub_total'){
				$subTotal = $item->value;
			}
		}
		$pickMoney = ($oderDetail->payment_method == 'os_offline') ? $subTotal : 0;
        $isFreeShip = ($data['freeShip'] > 0 || $pickMoney == 0) ? 1 : 0;
		$orders['order'] = array(
			'id' => $oderDetail->order_number,
			'pick_name' => $storeName,
			'pick_address' => $address,
			'pick_province' => $shopAddress['shopProvince'],
			'pick_district' => $shopAddress['shopDistrict'],
			"pick_tel" => $shopAddress['shopTelephone'],
			"tel" => $oderDetail->telephone,
			"name" => $oderDetail->firstname,
			"address" => $oderDetail->shipping_address_1,
			"province" => $oderDetail->shipping_country_name,
			"district" => $oderDetail->shipping_zone_name,
			"is_freeship" => $isFreeShip,
			"pick_date" => $pickDate,
			"pick_work_shift" => $data['pickWorkShift'],
			"deliver_work_shift" => $data['deliverWorkShift'],
			"pick_option" => $data['pickOption'],
			"pick_money" => ($oderDetail->payment_method == 'os_offline') ? $subTotal : 0,
			"note" => $data['note'],
			"value" => $subTotal
		);
		return $orders;
	}
}
