<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 4/27/2019
 * Time: 9:51 AM
 */


use api\model\dao\shop\ShopCustomerDao;
use api\model\Sconfig;


defined('_JEXEC') or die('Restricted access');
jimport('joomla.user.user');

class UsersApiResourceInfo extends ApiResource
{
    /**
     * @OA\Get(
     *     path="/api/users/info",
     *     tags={"User"},
     *     summary="Get user info",
     *     description="Get userinfo",
     *     operationId="get",
     *     security = { { "bearerAuth": {} } },
     *     @OA\RequestBody(
     *         required=true,
     *         description="Change password",
     *         @OA\JsonContent(ref="#/components/schemas/ChangePasswordForm"),
     *         @OA\MediaType(
     *            mediaType="multipart/form-data",
     *            @OA\Schema(ref="#/components/schemas/ChangePasswordForm"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful login",
     *         @OA\Schema(ref="#/components/schemas/ErrorModel"),
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Invalid request",
     *     )
     * )
     */
    public function get()
    {
        $db = JFactory::getDbo();
        $user = JFactory::getUser();
        $groups = JAccess::getGroupsByUser($user->id);
        $is_stock = in_array(10, $groups) || in_array(11, $groups);
        if ($is_stock) {
            $this->plugin->setResponse(array('group' => 'stock', 'address' => array('name' => $user->get('address'), 'phone' => $user->get('phone'))));
        } else {
            $dao = new ShopCustomerDao();
            $params = array(
                'where' => array(
                    'b.customer_id = ' . (int)$user->id
                )
            );
            $address = $dao->getDefaultAddress($params);
            // Get user info
            $sql = 'SELECT `name`, `username` as phone, `email` FROM #__users WHERE id = ' . (int)$user->id;
            $info = $db->setQuery($sql)->loadAssoc();

            // Get profile
            $sql = 'SELECT `profile_key`, `profile_value` FROM #__user_profiles WHERE user_id = ' . (int)$user->id;
            $profiles = $db->setQuery($sql)->loadAssocList();
            if ($profiles) {
                foreach ($profiles as $item) {
                    $key = str_replace('profile.', '', $item['profile_key']);
                    $info[$key] = $item['profile_value'];
                }
            }

            $config = new Sconfig();
            if($info['email']){
                if(strpos($info['email'], $config->email_prefix) === 0){
                    $info['email'] = '';
                }
            }


            $this->plugin->setResponse(array('address' => $address, 'info' => $info));
        }

        return true;
    }


}
