<?php
/**
 * @package Helix Ultimate Framework
 * @author JoomShaper https://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2018 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or Later
*/

defined ('_JEXEC') or die();

$data = $displayData;
$offcanvs_position = $displayData->params->get('offcanvas_position', 'right');

$feature_folder_path     = JPATH_THEMES . '/' . $data->template->template . '/features/';

include_once $feature_folder_path.'logo.php';
include_once $feature_folder_path.'social.php';
include_once $feature_folder_path.'contact.php';
include_once $feature_folder_path.'menu.php';

$output  = '';
?>
<header id="sp-header">
	<div id="header-top">
		<div class="container">
			<div class="row">
				<div id="sp-top1" class="col-lg-6">

					<?php
					$social = new HelixUltimateFeatureSocial($data->params);
					$top1 = '';
					if(isset($social->load_pos) && $social->load_pos == 'before')
					{
						$top1 .= $social->renderFeature();
						$top1 .= '<jdoc:include type="modules" name="top1" style="sp_xhtml" />';
					}
					else
					{
						$top1 .= '<jdoc:include type="modules" name="top1" style="sp_xhtml" />';
						$top1 .= $social->renderFeature();
					}
					echo $top1;
					?>
				</div>
				<div id="sp-top2" class="col-lg-6 text-right">
					<?php
					$contact = new HelixUltimateFeatureContact($data->params);
					$top2 = '';
					if(isset($contact->load_pos) && $contact->load_pos == 'before')
					{
						$top2 .= $contact->renderFeature();
						$top2 .= '<jdoc:include type="modules" name="top2" style="sp_xhtml" />';
					}
					else
					{
						$top2 .= '<jdoc:include type="modules" name="top2" style="sp_xhtml" />';
						$top2 .= $contact->renderFeature();
					}
					echo $top2;
					?>
				</div>
			</div>
		</div>
	</div>
	<div id="header-mid">
		<div class="container">
			<div class="row">
                <div class="navbar-logo col-md-2 col-sm-2">
                    <?php
                    $logo  = new HelixUltimateFeatureLogo($data->params);
                    if(isset($logo->load_pos) && $logo->load_pos == 'before')
                    {
                        $headerContent .= $logo->renderFeature();
                        $headerContent .= '<jdoc:include type="modules" name="logo" style="sp_xhtml" />';
                    }
                    else
                    {
                        $headerContent .= '<jdoc:include type="modules" name="logo" style="sp_xhtml" />';
                        $headerContent .= $logo->renderFeature();
                    }
                    echo $headerContent;
                    ?>
                </div>
                <div class="navbar-menu col-md-10 col-sm-10">
                    <?php if ($data->params['menu_type'] == 'offcanvas'): ?>

                    <div class="row">
                    <?php endif;?>
                        <?php
                        $bottomContent = '';
                        $menu    = new HelixUltimateFeatureMenu($data->params);
                        if(isset($menu->load_pos) && $menu->load_pos == 'before')
                        {
                            $bottomContent .= $menu->renderFeature();
                            $bottomContent .= '<jdoc:include type="modules" name="menu" style="sp_xhtml" />';
                        }
                        else
                        {
                            $bottomContent .= '<jdoc:include type="modules" name="menu" style="sp_xhtml" />';
                            $bottomContent .= $menu->renderFeature();
                        }
                        echo $bottomContent;
                        ?>
                    <?php if ($data->params['menu_type'] == 'offcanvas'): ?>
                    </div>
                    <?php endif;?>
                </div>
			</div>
		</div>
	</div>
	<div id="header-bottom">
		<div class="container">
            <div class="header-bottom-inner">
                <div class="row">
                    <jdoc:include type="modules" name="header-bottom" style="sp_xhtml" />
                </div>
            </div>
		</div>
	</div>
</header>
