<?php
/**
 * @package Helix Ultimate Framework
 * @author JoomShaper https://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2018 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or Later
*/

defined('_JEXEC') or die;
?>
<?php if($module->id == 153){ ?>
<!-- <h3 class="sppb-addon-title"><a href="index.php?Itemid=115">Tin tức mới nhất</a></h3> -->
<?php } ?>

<div class="row latestnews<?php echo $moduleclass_sfx; ?>">
<?php foreach ($list as $item) : $images = json_decode($item->images);?>
	<div class="col-md-3">
		<?php if ($images->image_intro): ?>
			<a href="<?php echo $item->link; ?>" itemprop="url">
				<span style="display: block; padding-bottom:75%; background: url(<?php echo $images->image_intro ?>) no-repeat center center / cover"></span>
			</a>
		<?php endif; ?>
		<h4 class="title">
			<a href="<?php echo $item->link; ?>">
				<?php echo $item->title ?>
			</a>
		</h4>
		<p><i class="fa fa-calendar"></i> <?php echo JHtml::_('date', $item->created, 'DATE_FORMAT_LC3'); ?></p>
		<p><?php echo JHtml::_('string.truncate', ($item->introtext), 160, true, false)?>	</p>
	</div>
<?php endforeach; ?>
<div class="view_all" style="clear:both;">
<?php if($module->id == 153){ ?>
	<a class="btn btn-default btn-primary" href="index.php?Itemid=115" >
	    <i class="fa fa-eye" aria-hidden="true"></i>
	    <span class="txt txt_detail">Xem tất cả</span>
	</a>
<?php } ?>
<?php if($module->id == 175){ ?>
	<a class="btn btn-default btn-primary" href="index.php?Itemid=400" >
	    <i class="fa fa-eye" aria-hidden="true"></i>
	    <span class="txt txt_detail">Xem tất cả</span>
	</a>
<?php } ?>
</div>
</div>
